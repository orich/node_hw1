const { check } = require('express-validator');
const path = require('path')

const createFileValidation =
[
  check('filename')
    .trim()
    .not()
    .isEmpty()
    .withMessage("Please specify 'filename' parameter")
    .custom(val => ['.log', '.txt', '.json', '.yaml', '.xml', '.js'].includes(path.extname(val)))
    .withMessage("You are only allowed to create files in the format log, txt, json, yaml, xml"),
  
  check('content')
    .trim()
    .not()
    .isEmpty()
    .withMessage("Please specify 'content' parameter")
];

module.exports = {
  createFileValidation,
}
