const express = require('express');

const bodyParser = require('body-parser');
const jsonParser = bodyParser.json();

const router = express.Router();
const { createFileValidation } = require('./routerValidation.js')
const { createFile, getFiles, getFile } = require('./filesService.js');

router.post('/',
  jsonParser, 
  createFileValidation,
  createFile);

router.get('/', getFiles);

router.get('/:filename', getFile);

// Other endpoints - put, delete.

module.exports = {
  filesRouter: router
};
