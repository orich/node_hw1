const fs = require('fs');
const path = require('path');
const { validationResult } = require('express-validator');

function createFile (req, res, next) {
  
  const filename = req.body.filename;

  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).send( { "message": errors.array()[0].msg });
  }
  
  const filePath = './files/' + filename;
  if (!fs.existsSync(filePath)) {
    fs.writeFile(filePath, req.body.content, function (err) {
      if (err) {
        throw err;
      } 
    });
  } else {
    return res.status(400).send({ "message": `File ${filename} already exists` });
  }

  res.status(200).send({ "message": "File created successfully" });
}

function getFiles (req, res, next) {
  let fileList = [];
  try {
    fileList = fs.readdirSync('./files/');     
  } catch (error) {
    throw error;
  }

  if (!fileList.length) {
    return res.status(400).send({ "message": `Client error` });
  }
  
  return res.status(200).send({
    "message": "Success",
    "files": fileList });
}

const getFile = (req, res, next) => {
  const filename = req.params.filename;
  const filePath = './files/' + filename;
  if (!fs.existsSync(filePath)) {
    return res.status(400).send({ "message": `No file with '${filename}' filename found` });
  }

  let content = '';
  let uploadedDate = '';
  
  try {
    content = fs.readFileSync(filePath, 'utf-8');
    uploadedDate = fs.statSync(filePath).birthtime;
  } catch (error) {
    throw error;
  }

  res.status(200).send({
    "message": "Success",
    "filename": filename,
    "content": content,
    "extension": path.extname(filename).slice(1),
    "uploadedDate": uploadedDate });
}

// Other functions - editFile, deleteFile

// path.extName('file.txt') ---> '.txt'
// fs.writeFile ({ flag: 'a' }) ---> adds content to the file

module.exports = {
  createFile,
  getFiles,
  getFile
}
